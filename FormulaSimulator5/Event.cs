﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace FormulaSimulator5
{
    public enum EventType
    {
        OnSetup,
        OnSetupComplete,
        OnSimulationTick,
        OnPositionClose,
        OnPositionOpen,
        OnRateChange,
        OnSimulationComplete,
        OnCustomEvent
    }

    public class Event
    {
        private EventType eventType;
        private String eventName;
        
        private List<ParsedCommand> parsedCommands;

        public Event(EventType _type, String _eventName)
        {
            eventType = _type;
            eventName = _eventName;

            parsedCommands = new List<ParsedCommand>();
        }

        public String getEventName()
        {
            return eventName;
        }

        public void addCommands(List<ParsedCommand> commands)
        {
            foreach (ParsedCommand command in commands)
                parsedCommands.Add(command);
        }
        
        public List<ParsedCommand> getCommands()
        {
            return parsedCommands;
        }
    }
}
