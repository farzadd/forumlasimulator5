﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormulaSimulator5
{
    class Settings
    {
        private List<Symbol> symbols = new List<Symbol>();

        public Settings()
        {
            Symbol s = new Symbol();
            s.name = "USD/CAD";
            s.pipValue = 1000;
            s.margin = 22;
            s.decimalPlaces = 4;

            symbols.Add(s);
        }

        public Symbol? getSymbol(String name)
        {
            foreach (Symbol s in symbols)
                if (s.name == name)
                    return s;

            return null;
        }
    }
}
