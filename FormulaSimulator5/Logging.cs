﻿using System;
using System.Collections.Generic;
using System.IO;

namespace FormulaSimulator5
{
    public enum logType
    {
        SimulationStarted = 0,
        OpenPosition,
        ClosePosition,
        SimulationSuccess,
        SimulationFailed,
        SimulationComplete,
        CustomLog,
        Debug
    }

    public class Logging
    {
        private string logFile;
        StreamWriter logStream;

        public Logging()
        {
            logFile = System.IO.Directory.GetCurrentDirectory() + "/Logs/" + "ID-NAME-" + new Random().Next(1000, 9999) + ".fsl";
            logStream = new StreamWriter(logFile);
        }

        ~Logging()
        {
        }

        public bool Log(logType type, String parameters)
        {
            logStream.WriteLineAsync((int)type + ";" + parameters);
            return true;
        }

        public void closeLog()
        {
            logStream.Flush();
            logStream.Close();
            logStream.Dispose();
        }
    }
}