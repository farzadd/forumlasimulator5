﻿namespace FormulaSimulator5
{
    partial class Main
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Main));
            this.tcMain = new System.Windows.Forms.TabControl();
            this.tcMain_SimSetup = new System.Windows.Forms.TabPage();
            this.label6 = new System.Windows.Forms.Label();
            this.bEditFormulas = new System.Windows.Forms.Button();
            this.cbFormulaName = new System.Windows.Forms.ComboBox();
            this.bAddToQueue = new System.Windows.Forms.Button();
            this.gb = new System.Windows.Forms.GroupBox();
            this.label5 = new System.Windows.Forms.Label();
            this.cbLimitSim = new System.Windows.Forms.CheckBox();
            this.label4 = new System.Windows.Forms.Label();
            this.dtpStartTime = new System.Windows.Forms.DateTimePicker();
            this.dtpEndTime = new System.Windows.Forms.DateTimePicker();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.tbInitialBalance = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.dtpEndDate = new System.Windows.Forms.DateTimePicker();
            this.dtpStartDate = new System.Windows.Forms.DateTimePicker();
            this.tcMain_Settings = new System.Windows.Forms.TabPage();
            this.bAbout = new System.Windows.Forms.Button();
            this.tcMain_LogViewer = new System.Windows.Forms.TabPage();
            this.tcMain_Status = new System.Windows.Forms.TabPage();
            this.dgvSimulationStatus = new System.Windows.Forms.DataGridView();
            this.dgvtbSimID = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtbStatus = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.dgvtbActions = new System.Windows.Forms.DataGridViewButtonColumn();
            this.dgvtbDetails = new System.Windows.Forms.DataGridViewButtonColumn();
            this.gbStatus = new System.Windows.Forms.GroupBox();
            this.lSimProgress = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.lSimInQueue = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.lSimStatus = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.bStartSimulation = new System.Windows.Forms.Button();
            this.tcMain.SuspendLayout();
            this.tcMain_SimSetup.SuspendLayout();
            this.gb.SuspendLayout();
            this.tcMain_Settings.SuspendLayout();
            this.tcMain_Status.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dgvSimulationStatus)).BeginInit();
            this.gbStatus.SuspendLayout();
            this.SuspendLayout();
            // 
            // tcMain
            // 
            this.tcMain.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tcMain.Controls.Add(this.tcMain_SimSetup);
            this.tcMain.Controls.Add(this.tcMain_Settings);
            this.tcMain.Controls.Add(this.tcMain_LogViewer);
            this.tcMain.Controls.Add(this.tcMain_Status);
            this.tcMain.Location = new System.Drawing.Point(12, 12);
            this.tcMain.Name = "tcMain";
            this.tcMain.SelectedIndex = 0;
            this.tcMain.Size = new System.Drawing.Size(571, 143);
            this.tcMain.TabIndex = 0;
            // 
            // tcMain_SimSetup
            // 
            this.tcMain_SimSetup.Controls.Add(this.label6);
            this.tcMain_SimSetup.Controls.Add(this.bEditFormulas);
            this.tcMain_SimSetup.Controls.Add(this.cbFormulaName);
            this.tcMain_SimSetup.Controls.Add(this.bAddToQueue);
            this.tcMain_SimSetup.Controls.Add(this.gb);
            this.tcMain_SimSetup.Controls.Add(this.label3);
            this.tcMain_SimSetup.Controls.Add(this.label2);
            this.tcMain_SimSetup.Controls.Add(this.tbInitialBalance);
            this.tcMain_SimSetup.Controls.Add(this.label1);
            this.tcMain_SimSetup.Controls.Add(this.dtpEndDate);
            this.tcMain_SimSetup.Controls.Add(this.dtpStartDate);
            this.tcMain_SimSetup.Location = new System.Drawing.Point(4, 22);
            this.tcMain_SimSetup.Name = "tcMain_SimSetup";
            this.tcMain_SimSetup.Padding = new System.Windows.Forms.Padding(3);
            this.tcMain_SimSetup.Size = new System.Drawing.Size(563, 117);
            this.tcMain_SimSetup.TabIndex = 0;
            this.tcMain_SimSetup.Text = "Simulation Setup";
            this.tcMain_SimSetup.UseVisualStyleBackColor = true;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(286, 61);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(47, 13);
            this.label6.TabIndex = 12;
            this.label6.Text = "Formula:";
            // 
            // bEditFormulas
            // 
            this.bEditFormulas.Location = new System.Drawing.Point(474, 56);
            this.bEditFormulas.Name = "bEditFormulas";
            this.bEditFormulas.Size = new System.Drawing.Size(83, 23);
            this.bEditFormulas.TabIndex = 7;
            this.bEditFormulas.Text = "Edit Formulas";
            this.bEditFormulas.UseVisualStyleBackColor = true;
            // 
            // cbFormulaName
            // 
            this.cbFormulaName.FormattingEnabled = true;
            this.cbFormulaName.Items.AddRange(new object[] {
            "sample"});
            this.cbFormulaName.Location = new System.Drawing.Point(350, 58);
            this.cbFormulaName.Name = "cbFormulaName";
            this.cbFormulaName.Size = new System.Drawing.Size(118, 21);
            this.cbFormulaName.TabIndex = 6;
            // 
            // bAddToQueue
            // 
            this.bAddToQueue.Location = new System.Drawing.Point(350, 85);
            this.bAddToQueue.Name = "bAddToQueue";
            this.bAddToQueue.Size = new System.Drawing.Size(207, 23);
            this.bAddToQueue.TabIndex = 8;
            this.bAddToQueue.Text = "Add to Simulation Queue";
            this.bAddToQueue.UseVisualStyleBackColor = true;
            this.bAddToQueue.Click += new System.EventHandler(this.bAddToQueue_Click);
            // 
            // gb
            // 
            this.gb.Controls.Add(this.label5);
            this.gb.Controls.Add(this.cbLimitSim);
            this.gb.Controls.Add(this.label4);
            this.gb.Controls.Add(this.dtpStartTime);
            this.gb.Controls.Add(this.dtpEndTime);
            this.gb.Location = new System.Drawing.Point(9, 32);
            this.gb.Name = "gb";
            this.gb.Size = new System.Drawing.Size(261, 78);
            this.gb.TabIndex = 8;
            this.gb.TabStop = false;
            this.gb.Text = "                                      ";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(8, 48);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(55, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "End Time:";
            // 
            // cbLimitSim
            // 
            this.cbLimitSim.AutoSize = true;
            this.cbLimitSim.Location = new System.Drawing.Point(11, -1);
            this.cbLimitSim.Name = "cbLimitSim";
            this.cbLimitSim.Size = new System.Drawing.Size(117, 17);
            this.cbLimitSim.TabIndex = 1;
            this.cbLimitSim.Text = "Limit Simulation To:";
            this.cbLimitSim.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(8, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(58, 13);
            this.label4.TabIndex = 9;
            this.label4.Text = "Start Time:";
            // 
            // dtpStartTime
            // 
            this.dtpStartTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpStartTime.Location = new System.Drawing.Point(70, 19);
            this.dtpStartTime.Name = "dtpStartTime";
            this.dtpStartTime.ShowUpDown = true;
            this.dtpStartTime.Size = new System.Drawing.Size(97, 20);
            this.dtpStartTime.TabIndex = 2;
            // 
            // dtpEndTime
            // 
            this.dtpEndTime.Format = System.Windows.Forms.DateTimePickerFormat.Time;
            this.dtpEndTime.Location = new System.Drawing.Point(70, 45);
            this.dtpEndTime.Name = "dtpEndTime";
            this.dtpEndTime.ShowUpDown = true;
            this.dtpEndTime.Size = new System.Drawing.Size(97, 20);
            this.dtpEndTime.TabIndex = 3;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(286, 35);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(58, 13);
            this.label3.TabIndex = 5;
            this.label3.Text = "Balance: $";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(286, 9);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 13);
            this.label2.TabIndex = 3;
            this.label2.Text = "End Date:";
            // 
            // tbInitialBalance
            // 
            this.tbInitialBalance.Location = new System.Drawing.Point(350, 32);
            this.tbInitialBalance.Name = "tbInitialBalance";
            this.tbInitialBalance.Size = new System.Drawing.Size(118, 20);
            this.tbInitialBalance.TabIndex = 5;
            this.tbInitialBalance.Text = "100";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 13);
            this.label1.TabIndex = 2;
            this.label1.Text = "Start Date:";
            // 
            // dtpEndDate
            // 
            this.dtpEndDate.Location = new System.Drawing.Point(350, 6);
            this.dtpEndDate.Name = "dtpEndDate";
            this.dtpEndDate.Size = new System.Drawing.Size(200, 20);
            this.dtpEndDate.TabIndex = 4;
            // 
            // dtpStartDate
            // 
            this.dtpStartDate.Location = new System.Drawing.Point(70, 6);
            this.dtpStartDate.Name = "dtpStartDate";
            this.dtpStartDate.Size = new System.Drawing.Size(200, 20);
            this.dtpStartDate.TabIndex = 0;
            // 
            // tcMain_Settings
            // 
            this.tcMain_Settings.Controls.Add(this.bAbout);
            this.tcMain_Settings.Location = new System.Drawing.Point(4, 22);
            this.tcMain_Settings.Name = "tcMain_Settings";
            this.tcMain_Settings.Padding = new System.Windows.Forms.Padding(3);
            this.tcMain_Settings.Size = new System.Drawing.Size(563, 117);
            this.tcMain_Settings.TabIndex = 1;
            this.tcMain_Settings.Text = "Settings";
            this.tcMain_Settings.UseVisualStyleBackColor = true;
            // 
            // bAbout
            // 
            this.bAbout.Location = new System.Drawing.Point(461, 85);
            this.bAbout.Name = "bAbout";
            this.bAbout.Size = new System.Drawing.Size(96, 23);
            this.bAbout.TabIndex = 0;
            this.bAbout.Text = "About";
            this.bAbout.UseVisualStyleBackColor = true;
            this.bAbout.Click += new System.EventHandler(this.bAbout_Click);
            // 
            // tcMain_LogViewer
            // 
            this.tcMain_LogViewer.Location = new System.Drawing.Point(4, 22);
            this.tcMain_LogViewer.Name = "tcMain_LogViewer";
            this.tcMain_LogViewer.Size = new System.Drawing.Size(563, 117);
            this.tcMain_LogViewer.TabIndex = 3;
            this.tcMain_LogViewer.Text = "Log Viewer";
            this.tcMain_LogViewer.UseVisualStyleBackColor = true;
            // 
            // tcMain_Status
            // 
            this.tcMain_Status.Controls.Add(this.dgvSimulationStatus);
            this.tcMain_Status.Location = new System.Drawing.Point(4, 22);
            this.tcMain_Status.Name = "tcMain_Status";
            this.tcMain_Status.Size = new System.Drawing.Size(563, 117);
            this.tcMain_Status.TabIndex = 2;
            this.tcMain_Status.Text = "Status";
            this.tcMain_Status.UseVisualStyleBackColor = true;
            // 
            // dgvSimulationStatus
            // 
            this.dgvSimulationStatus.AllowUserToAddRows = false;
            this.dgvSimulationStatus.AllowUserToDeleteRows = false;
            this.dgvSimulationStatus.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvSimulationStatus.ColumnHeadersBorderStyle = System.Windows.Forms.DataGridViewHeaderBorderStyle.None;
            this.dgvSimulationStatus.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvSimulationStatus.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.dgvtbSimID,
            this.dgvtbStatus,
            this.dgvtbActions,
            this.dgvtbDetails});
            this.dgvSimulationStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dgvSimulationStatus.Location = new System.Drawing.Point(0, 0);
            this.dgvSimulationStatus.Name = "dgvSimulationStatus";
            this.dgvSimulationStatus.ReadOnly = true;
            this.dgvSimulationStatus.RowHeadersVisible = false;
            this.dgvSimulationStatus.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dgvSimulationStatus.Size = new System.Drawing.Size(563, 117);
            this.dgvSimulationStatus.TabIndex = 0;
            // 
            // dgvtbSimID
            // 
            this.dgvtbSimID.HeaderText = "Simulation ID";
            this.dgvtbSimID.Name = "dgvtbSimID";
            this.dgvtbSimID.ReadOnly = true;
            // 
            // dgvtbStatus
            // 
            this.dgvtbStatus.HeaderText = "Status";
            this.dgvtbStatus.Name = "dgvtbStatus";
            this.dgvtbStatus.ReadOnly = true;
            // 
            // dgvtbActions
            // 
            this.dgvtbActions.HeaderText = "Actions";
            this.dgvtbActions.Name = "dgvtbActions";
            this.dgvtbActions.ReadOnly = true;
            // 
            // dgvtbDetails
            // 
            this.dgvtbDetails.HeaderText = "Details";
            this.dgvtbDetails.Name = "dgvtbDetails";
            this.dgvtbDetails.ReadOnly = true;
            // 
            // gbStatus
            // 
            this.gbStatus.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.gbStatus.Controls.Add(this.lSimProgress);
            this.gbStatus.Controls.Add(this.label11);
            this.gbStatus.Controls.Add(this.lSimInQueue);
            this.gbStatus.Controls.Add(this.label9);
            this.gbStatus.Controls.Add(this.lSimStatus);
            this.gbStatus.Controls.Add(this.label7);
            this.gbStatus.Controls.Add(this.bStartSimulation);
            this.gbStatus.Location = new System.Drawing.Point(589, 26);
            this.gbStatus.Name = "gbStatus";
            this.gbStatus.Size = new System.Drawing.Size(172, 129);
            this.gbStatus.TabIndex = 1;
            this.gbStatus.TabStop = false;
            this.gbStatus.Text = "Simulation Status/Controls";
            // 
            // lSimProgress
            // 
            this.lSimProgress.AutoSize = true;
            this.lSimProgress.Location = new System.Drawing.Point(64, 48);
            this.lSimProgress.Name = "lSimProgress";
            this.lSimProgress.Size = new System.Drawing.Size(92, 13);
            this.lSimProgress.TabIndex = 6;
            this.lSimProgress.Text = "SIM_PROGRESS";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(7, 48);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(51, 13);
            this.label11.TabIndex = 5;
            this.label11.Text = "Progress:";
            // 
            // lSimInQueue
            // 
            this.lSimInQueue.AutoSize = true;
            this.lSimInQueue.Location = new System.Drawing.Point(64, 32);
            this.lSimInQueue.Name = "lSimInQueue";
            this.lSimInQueue.Size = new System.Drawing.Size(87, 13);
            this.lSimInQueue.TabIndex = 4;
            this.lSimInQueue.Text = "SIM_IN_QUEUE";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(6, 32);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(52, 13);
            this.label9.TabIndex = 3;
            this.label9.Text = "In-queue:";
            // 
            // lSimStatus
            // 
            this.lSimStatus.AutoSize = true;
            this.lSimStatus.Location = new System.Drawing.Point(52, 16);
            this.lSimStatus.Name = "lSimStatus";
            this.lSimStatus.Size = new System.Drawing.Size(75, 13);
            this.lSimStatus.TabIndex = 2;
            this.lSimStatus.Text = "SIM_STATUS";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(6, 16);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(40, 13);
            this.label7.TabIndex = 1;
            this.label7.Text = "Status:";
            // 
            // bStartSimulation
            // 
            this.bStartSimulation.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.bStartSimulation.Location = new System.Drawing.Point(6, 93);
            this.bStartSimulation.Name = "bStartSimulation";
            this.bStartSimulation.Size = new System.Drawing.Size(160, 23);
            this.bStartSimulation.TabIndex = 9;
            this.bStartSimulation.Text = "Start Simulation";
            this.bStartSimulation.UseVisualStyleBackColor = true;
            this.bStartSimulation.Click += new System.EventHandler(this.bStartSimulation_Click);
            // 
            // Main
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(773, 167);
            this.Controls.Add(this.gbStatus);
            this.Controls.Add(this.tcMain);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "Main";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Formula Simulator 5";
            this.Load += new System.EventHandler(this.Main_Load);
            this.tcMain.ResumeLayout(false);
            this.tcMain_SimSetup.ResumeLayout(false);
            this.tcMain_SimSetup.PerformLayout();
            this.gb.ResumeLayout(false);
            this.gb.PerformLayout();
            this.tcMain_Settings.ResumeLayout(false);
            this.tcMain_Status.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.dgvSimulationStatus)).EndInit();
            this.gbStatus.ResumeLayout(false);
            this.gbStatus.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TabControl tcMain;
        private System.Windows.Forms.TabPage tcMain_SimSetup;
        private System.Windows.Forms.TabPage tcMain_Settings;
        private System.Windows.Forms.GroupBox gbStatus;
        private System.Windows.Forms.TabPage tcMain_Status;
        private System.Windows.Forms.TabPage tcMain_LogViewer;
        private System.Windows.Forms.DataGridView dgvSimulationStatus;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtbSimID;
        private System.Windows.Forms.DataGridViewTextBoxColumn dgvtbStatus;
        private System.Windows.Forms.DataGridViewButtonColumn dgvtbActions;
        private System.Windows.Forms.DataGridViewButtonColumn dgvtbDetails;
        private System.Windows.Forms.Button bAbout;
        private System.Windows.Forms.Button bStartSimulation;
        private System.Windows.Forms.TextBox tbInitialBalance;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DateTimePicker dtpEndDate;
        private System.Windows.Forms.DateTimePicker dtpStartDate;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.GroupBox gb;
        private System.Windows.Forms.DateTimePicker dtpEndTime;
        private System.Windows.Forms.DateTimePicker dtpStartTime;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.CheckBox cbLimitSim;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button bAddToQueue;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Button bEditFormulas;
        private System.Windows.Forms.ComboBox cbFormulaName;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label lSimInQueue;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label lSimStatus;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label lSimProgress;
    }
}

