﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormulaSimulator5
{
    public struct Symbol
    {
        public string name;
        public int pipValue;
        public int decimalPlaces;
        public int margin;
    }

    public struct Rate
    {
        public string symbol;
        public double sell;
        public double buy;
        public DateTime date;
    }

    public enum CloseReason
    {
        MANUAL, STOP_LOSS, LIMIT_PROFIT
    }

    public enum orderType
    {
        Open = 0,
        CloseAll,
        ChangeLimitAll
    }

    public struct Order
    {
        public orderType type;
        public Position position;
    }

    public enum positionType
    {
        Sell = 0,
        Buy,
    }

    public class Position
    {
        public Symbol symbol;
        public int amount;
        public positionType type;
        public double openRate;
        public double limitRate;
        public double stopRate;
        public DateTime openDate;

        public Position(Symbol _symbol, int _amount, positionType _type, double _openRate, double _limitRate = 0, double _stopRate = 0)
        {
            symbol = _symbol;
            amount = _amount;
            type = _type;
            openRate = _openRate;
            limitRate = _limitRate;
            stopRate = _stopRate;
        }

        public double calculateProfit(Rate currentRate)
        {
            double profit = 0;
            if (type == positionType.Sell)
                profit = (openRate - currentRate.buy) * amount * symbol.pipValue;
            else
                profit = (currentRate.sell - openRate) * amount * symbol.pipValue;

            return Math.Round(profit, 2);
        }

        public double calculateUsedMargin()
        {
            return amount * symbol.margin;
        }

        public bool shouldOpen(Rate currentRate)
        {
            double rateDifference;

            // Calculate the difference between the rate now and openRate
            if (type == positionType.Sell)
                rateDifference = openRate - currentRate.sell;
            else
                rateDifference = currentRate.buy - openRate;

            // The difference must be equal or slighly greater to open (for error/skipping)
            if (rateDifference >= 0 && rateDifference < 0.0003)         //TODO : MUST FIX TO WORK WITH ALL CUR
                return true;

            return false;
        }

        public CloseReason? shouldClose(Rate currentRate)
        {
            // Type is sell
            if (type == positionType.Sell)
            {
                if (limitRate >= currentRate.buy)
                    return CloseReason.LIMIT_PROFIT;
                else if (stopRate >= currentRate.buy)       // Double check this with big boy ali
                    return CloseReason.STOP_LOSS;
            }
            else
            // Type is buy
            {
                if (limitRate <= currentRate.sell)
                    return CloseReason.LIMIT_PROFIT;
                else if (stopRate <= currentRate.sell)       // Double check this with big boy ali
                    return CloseReason.STOP_LOSS;
            }

            return null;
        }
    }

    public class Server
    {
        private Logging logging;

        private double accountBalance = 0;
        private List<Rate> currentRates = new List<Rate>();
        private List<Position> openPositions = new List<Position>();
        private List<Order> orders = new List<Order>();
        public DateTime simulationDate;

        public Server(Logging _logging)
        {
            logging = _logging;
        }

        public void tick()
        {
            foreach (Position pos in openPositions)
            {
                // Get the current rate
                Rate rate = getCurrentRate(pos.symbol.name);

                // Retrieve the close reason
                CloseReason? closeReason = pos.shouldClose(rate);

                // Should the position be closed?
                if (closeReason != null)
                {
                    // Close it
                    closePosition(pos, (CloseReason) closeReason);
                }
            }

            foreach (Order order in orders)
            {
                // Get the current rate
                Rate rate = getCurrentRate(order.position.symbol.name);

                // Switch on Order type
                switch (order.type)
                {
                    case orderType.Open:
                        if (order.position.shouldOpen(rate))
                            openPosition(order.position, rate);
                        break;
                    case orderType.CloseAll:
                        break;
                    case orderType.ChangeLimitAll:
                        break;
                    default:
                        break;
                }
            }
        }

        public double getBalance()
        {
            return accountBalance;
        }

        public double getEquity()
        {
            return Math.Round(accountBalance + getTotalProfit(), 2);
        }

        public double getUseableMargin()
        {
            return Math.Round(getEquity() - getUsedMargin(), 2);
        }

        public double getUsedMargin()
        {
            double total_sell = 0, total_buy = 0;

            foreach (Position pos in openPositions)
                if (pos.type == positionType.Sell)
                    total_sell += pos.calculateUsedMargin();
                else
                    total_buy += pos.calculateUsedMargin();
            
            return Math.Round(Math.Max(total_sell, total_buy), 2);
        }

        private double getTotalProfit()
        {
            double total = 0;

            foreach (Position pos in openPositions)
                total += pos.calculateProfit(getCurrentRate(pos.symbol.name));

            return total;
        }

        public Rate getCurrentRate(String symbol)
        {
            for (int i = 0; i < currentRates.Count; i++)
            {
                if (currentRates[i].symbol == symbol)
                    return currentRates[i];
            }

            return currentRates[0];
        }

        public void updateRate(Rate newRate)
        {
            for (int i = 0; i < currentRates.Count; i ++)
            {
                if (currentRates[i].symbol == newRate.symbol)
                {
                    currentRates[i] = newRate;
                    return;
                }
            }

            currentRates.Add(newRate);
        }

        public void addOrder(Order order)
        {
            orders.Add(order);
        }

        public void openPosition(Position position, Rate rate)
        {
            // Set the date
            position.openDate = rate.date;

            // Log it
            // TODO

            // Add it to the list
            openPositions.Add(position);

            // Call the event
            // TODO
        }

        public void closePosition(Position position, CloseReason reason)
        {
            // Get the current rate
            Rate rate = getCurrentRate(position.symbol.name);

            // Calculate the profit/loss
            double pl = position.calculateProfit(rate);

            // Add the profit to the balance
            accountBalance += pl;

            // Log it
            // TODO
            
            // Remove it from the open positions
            openPositions.Remove(position);

            // Call the event
            // TODO
        }
    }
}
