﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Runtime.InteropServices;
using Microsoft.Win32;

namespace FormulaSimulator5
{
    static class Program
    {
        [DllImport("kernel32.dll")]
        static extern IntPtr GetConsoleWindow();

        [DllImport("user32.dll")]
        static extern bool ShowWindow(IntPtr hWnd, int nCmdShow);

        const int SW_HIDE = 0;
        const int SW_SHOW = 5;

        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            PrintHeader();

            bool bConsole = false;
            bool bUI = true;
            foreach (string arg in args)
            {
                if (arg == "-console")
                    bConsole = true;
                else if (arg == "-noUI")
                {
                    bUI = false;
                    bConsole = true;
                }
            }

            Console.WriteLine("Reading settings...");
            System.Threading.Thread.Sleep(100);
            Console.WriteLine("Executing default scripts...");
            System.Threading.Thread.Sleep(100);
            Console.WriteLine("Checking file integrities...");
            System.Threading.Thread.Sleep(100);
            Console.WriteLine("Setting up UI...");
            System.Threading.Thread.Sleep(100);

            if (!bConsole)
            {
                var handle = GetConsoleWindow();
                ShowWindow(handle, SW_HIDE);
            }

            if (bUI)
            {
                Console.WriteLine("Displaying UI...");
                System.Threading.Thread.Sleep(100);
                Console.Clear();
                PrintHeader();
                Console.WriteLine("                       UI Mode Active - Input disabled.");
                Application.EnableVisualStyles();
                Application.SetCompatibleTextRenderingDefault(false);
                Application.Run(new Main());
            }
            else
            {
                Console.WriteLine("Entering Console Mode...");
                System.Threading.Thread.Sleep(100);
                Console.Clear();
                PrintHeader();
                Console.Write("> ");
                Console.ReadLine();
            }
        }

        static void PrintHeader()
        {
            Console.WriteLine("                    =====================================");
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("                      Formula Simulator 5 - " + System.Reflection.Assembly.GetExecutingAssembly().GetName().Version.ToString());
            Console.ForegroundColor = ConsoleColor.White;
            Console.WriteLine("                    =====================================");
        }
    }
}
