﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormulaSimulator5
{
    public struct SimulationParameters
    {
        public string FormulaName;
        public int InitialBalance;
        public bool LimitSim;

        public DateTime StartDate;
        public DateTime EndDate;
        public DateTime OpenTime;
        public DateTime CloseTime;
    }

    public enum SimulationStatus
    {
        NOT_STARTED,
        IN_PROGRESS,
        PAUSED,
        COMPLETE
    }

    public enum SimulationResults
    {
        STOPPED_BY_USER,
        FAILED_RAN_OUT_OF_EQUITY,
        FAILED_RAN_OUT_OF_MARGIN,
        SUCCESS
    }

    public class Simulation
    {
        public Logging logging;
        private CommandParser commandParser;
        private Server serverSide;
        private Settings settings;
        private List<Event> events;
        private List<Symbol> subscribedSymbols;

        private SimulationParameters parameters;
        private SimulationStatus simStatus;
        private SimulationResults simResults;

        public Simulation(SimulationParameters _parameters)
        {
            logging = new Logging();
            commandParser = new CommandParser();
            serverSide = new Server(logging);
            settings = new Settings();
            events = new List<Event>();
            subscribedSymbols = new List<Symbol>();
            parameters = _parameters;

            simStatus = SimulationStatus.NOT_STARTED;
            simResults = SimulationResults.STOPPED_BY_USER;

            logging.Log(logType.SimulationStarted, "");

            readFormula();
        }

        public void tickSimulation()
        {
            serverSide.tick();
            executeEvent("OnSetup");
            executeEvent("OnSetupComplete");
            logging.closeLog();
        }

        public SimulationStatus getStatus()
        {
            return simStatus;
        }

        public void setStatus(SimulationStatus status)
        {
            simStatus = status;
        }

        public SimulationResults getResults()
        {
            return simResults;
        }

        public void readFormula()
        {
            // Read/Parse the setup information
            Event newEvent = new Event(EventType.OnSetup, "OnSetup");
            newEvent.addCommands(readEvent("OnSetup"));
            events.Add(newEvent);

            // Read/Parse all other events events
            // -- OnSetupComplete
            newEvent = new Event(EventType.OnSetupComplete, "OnSetupComplete");
            newEvent.addCommands(readEvent("OnSetupComplete"));
            events.Add(newEvent);
        }

        private List<ParsedCommand> readEvent(String eventName)
        {
            logging.Log(logType.Debug, "Reading Event: " + eventName);

            List<ParsedCommand> commandList = new List<ParsedCommand>();
            try
            {
                using (StreamReader sr = new StreamReader(System.IO.Directory.GetCurrentDirectory() + "/Formulas/" + parameters.FormulaName + "/Event_" + eventName + ".txt"))
                {
                    String sFile = sr.ReadToEnd();
                    String[] lines = sFile.Split(new string[] { Environment.NewLine }, StringSplitOptions.None);
                    foreach (String line in lines)
                    {
                        ParsedCommand? pCommand = commandParser.parseCommand(line);
                        if (pCommand != null)
                            commandList.Add((ParsedCommand)pCommand);
                    }
                }
            }
            catch (Exception ex)
            {
                logging.Log(logType.Debug, "ERROR: " + ex.Message);
            }
            return commandList;
        }

        public void executeEvent(String eventName)
        {
            foreach (Event evnt in events)
            {
                if (evnt.getEventName() != eventName)
                {
                    continue;
                }

                foreach (ParsedCommand command in evnt.getCommands())
                {
                    switch (command.type)
                    {
                        case CommandType.CREATE_ORDER:
                            logging.Log(logType.Debug, "Create Order function called " + command.arguments[0]);

                            Order newOrder = new Order();
                            if (command.arguments[0].ToString().Equals("OPEN"))
                            {
                                newOrder.type = orderType.Open;
                            }

                            Symbol? symbol = settings.getSymbol((String) command.arguments[1]);
                            if (symbol == null)
                            {
                                logging.Log(logType.Debug, "Symbol " + command.arguments[1] + " not found!");
                                break;
                            }

                            int amount;
                            double openRate, limitRate, stopRate;

                            if (!int.TryParse(command.arguments[2].ToString(), out amount))
                            {
                                logging.Log(logType.Debug, "Could not convert amount (" + command.arguments[2].ToString() + ") to a int");
                                break;
                            }
                            else if (!double.TryParse(command.arguments[4].ToString(), out openRate))
                            {
                                logging.Log(logType.Debug, "Could not convert openRate (" + command.arguments[4].ToString() + ") to a double");
                                break;
                            }
                            else if (!double.TryParse(command.arguments[5].ToString(), out limitRate))
                            {
                                logging.Log(logType.Debug, "Could not convert limitRate (" + command.arguments[5].ToString() + ") to a double");
                                break;
                            }
                            else if (!double.TryParse(command.arguments[6].ToString(), out stopRate))
                            {
                                logging.Log(logType.Debug, "Could not convert stopRate (" + command.arguments[6].ToString() + ") to a double");
                                break;
                            }

                            newOrder.position = new Position((Symbol) symbol,
                                amount,
                                (command.arguments[3].ToString().Equals("SELL")) ? positionType.Sell : positionType.Buy,
                                openRate,
                                limitRate,
                                stopRate);

                            serverSide.addOrder(newOrder);
                            break;
                        case CommandType.SUBSCRIBE_RATE:
                            logging.Log(logType.Debug, "Subscribing to rate " + command.arguments[0]);

                            Symbol? symbolName = settings.getSymbol((String) command.arguments[0]);
                            if (symbolName == null)
                            {
                                logging.Log(logType.Debug, "Symbol " + command.arguments[0] + " not found!");
                                break;
                            }

                            subscribeToSymbol((Symbol) symbolName);

                            break;
                        case CommandType.NEW_VARIABLE:
                            logging.Log(logType.Debug, "Unexpected function called: " + command.type.ToString() + " [Outside of OnSetup]");
                            break;
                        default:
                            logging.Log(logType.Debug, "Unimplemented/unknown function called: " + command.type.ToString());
                            break;
                    }
                }
            }
        }

        private void subscribeToSymbol(Symbol symbol)
        {
            subscribedSymbols.Add(symbol);
        }
    }
}
