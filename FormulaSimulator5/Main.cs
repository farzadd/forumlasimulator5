﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace FormulaSimulator5
{
    public partial class Main : Form
    {
        public struct SimPack
        {
            public Simulation simulation;
            public BackgroundWorker backgroundworker;
        }

        public Main()
        {
            InitializeComponent();
        }

        public List<SimPack> simulations;

        private void Main_Load(object sender, EventArgs e)
        {
            simulations = new List<SimPack>();

            lSimStatus.Text = "Waiting...";
            lSimInQueue.Text = "0";
            lSimProgress.Text = "0%";

            cbFormulaName.SelectedIndex = 0;
        }

        private void bAbout_Click(object sender, EventArgs e)
        {
            About fAbout = new About();
            fAbout.ShowDialog();
        }

        private void bStartSimulation_Click(object sender, EventArgs e)
        {
            foreach (SimPack simpack in simulations)
            {
                simpack.simulation.setStatus(SimulationStatus.IN_PROGRESS);
                UI_UpdateStatus();

                simpack.backgroundworker.RunWorkerAsync();
            }
        }

        void bwSimulation_DoWork(object sender, DoWorkEventArgs e)
        {
            foreach (SimPack s in simulations)
            {
                if (s.backgroundworker == sender )
                {
                    s.simulation.tickSimulation();
                }
            }
        }

        private void bAddToQueue_Click(object sender, EventArgs e)
        {
            // Create the simulation parameters
            SimulationParameters simParameters = new SimulationParameters();
            // -- Get the initial balance
            int initBalance;
            if (!int.TryParse(tbInitialBalance.Text, out initBalance))
            {
                MessageBox.Show("The initial balance must be a number!");
                return;
            }

            // Inital Balance
            simParameters.InitialBalance = initBalance;

            // -- Get the date times
            simParameters.StartDate = dtpStartDate.Value;
            simParameters.EndDate = dtpEndDate.Value;
            simParameters.OpenTime = dtpStartTime.Value;
            simParameters.CloseTime = dtpEndTime.Value;

            // -- Get the etc
            simParameters.FormulaName = cbFormulaName.Text;
            simParameters.LimitSim = cbLimitSim.Checked;

            // Create the simulation with those parameters
            Simulation sim = new Simulation(simParameters);

            // Create the background worker
            BackgroundWorker bwSimulation = new BackgroundWorker();
            bwSimulation.DoWork += bwSimulation_DoWork;

            // Create a new simulation package
            SimPack simpack = new SimPack();
            simpack.simulation = sim;
            simpack.backgroundworker = bwSimulation;
            
            simulations.Add(simpack);

            // Update the status
            dgvSimulationStatus.Rows.Add("1", "Ready to Start", "Start", "View Details");
            UI_UpdateStatus();
        }

        public void UI_UpdateStatus()
        {
            lSimStatus.Text = "Started";
            lSimInQueue.Text = simulations.Count.ToString();
            lSimProgress.Text = "0%";

            for (int i = 0; i < dgvSimulationStatus.Rows.Count; i ++)
            {
                dgvSimulationStatus.Rows[i].Cells[1].Value = simulations[i].simulation.getStatus().ToString().Replace("_", " ");
            }
        }
    }
}
