﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FormulaSimulator5
{
    public enum CommandType
    {
        CALL_EVENT,
        OPEN_POSITION,
        CLOSE_POSITION,
        CLOSE_ALL,
        SET_LIMIT,
        SET_LIMIT_ALL,
        CREATE_ORDER,
        SUBSCRIBE_RATE,
        NEW_VARIABLE,
        UNKNOWN_COMMAND
    }

    public struct ParsedCommand
    {
        public CommandType type;
        public List<Object> arguments;
    }

    public struct ParsedVarible
    {
        public String name;
        public String value;
    }

    public class CommandParser
    {
        private List<ParsedVarible> parsedVaribles = new List<ParsedVarible>();

        public ParsedCommand? parseCommand(String command)
        {
            // Check for comments
            if (command.StartsWith("//"))
                return null;

            // Create a parsedCommand object
            ParsedCommand parsedCommand = new ParsedCommand();

            // Check for syntax problems
            if (!command.EndsWith(")") || !command.Contains("("))
                return null;

            // Remove the ending bracket
            command = command.Remove(command.Length - 1, 1);

            // Split up function name and arguments for parsing
            String[] parts = command.Split(new String[] { "(" }, 2, StringSplitOptions.RemoveEmptyEntries);
            if (parts.Length < 2)
                return null;

            // Get function name and arguments
            String funcName = parts[0].Trim();
            String[] arguments = parts[1].Split(new String[] { "," }, StringSplitOptions.RemoveEmptyEntries);

            parsedCommand.type = GetCommandType(funcName);
            parsedCommand.arguments = new List<Object>();

            for (int i = 0; i < arguments.Length; i++)
            {
                arguments[i] = processArgument(arguments[i]);
                parsedCommand.arguments.Add(arguments[i]);
            }

            if (parsedCommand.type == CommandType.NEW_VARIABLE && arguments.Length == 2)
            {
                ParsedVarible newVar = new ParsedVarible();
                newVar.name = parsedCommand.arguments[0].ToString();
                newVar.value = parsedCommand.arguments[1].ToString();

                parsedVaribles.Add(newVar);

                return null;
            }

            return parsedCommand;
        }

        private String processArgument(String argument, bool skipVars = false)
        {
            argument = argument.Trim();

            // Find and replace occurrences of varibles
            if (!skipVars)
            {
                foreach (ParsedVarible var in parsedVaribles)
                {
                    if (argument.Contains("%" + var.name + "%"))
                    {
                        argument = argument.Replace("%" + var.name + "%", var.value);
                    }
                }
            }

            // Do math operations
            // -- Addition
            while (argument.Contains("+"))
            {
                String[] parts = argument.Split(new String[] {"+"}, 2, StringSplitOptions.RemoveEmptyEntries);
                if (parts.Length == 2)
                {
                    double leftSide, rightSide;
                    if (!double.TryParse(processArgument(parts[0], true), out leftSide))
                    {
                        leftSide = 0;
                    }

                    if (!double.TryParse(processArgument(parts[1], true), out rightSide))
                    {
                        rightSide = 0;
                    }

                    argument = (leftSide + rightSide).ToString();
                }
            }

            return argument;
        }

        private static CommandType GetCommandType(String funcName)
        {
            if (funcName == "CreateOrder")
            {
                return CommandType.CREATE_ORDER;
            }
            else if (funcName == "SubscribeRate")
            {
                return CommandType.SUBSCRIBE_RATE;
            }
            else if (funcName == "NewVariable")
            {
                return CommandType.NEW_VARIABLE;
            }

            return CommandType.UNKNOWN_COMMAND;
        }
    }
}
