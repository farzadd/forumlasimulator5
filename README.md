# README #

# To-do #
	-> Formula Selector
	-> Formula Editor / Creator
		-> CallEvent(EventName, Arguments)
		-> OpenPosition(Rate, Limit, StopLoss)
		-> ClosePosition(PositionID)
		-> CloseAll()
		-> SetLimit(PositionID, NewLimit)
		-> SetLimitAll(NewLimit)
		-> CreateOrder(OrderType, Position)
		-> Syntax: if, compare, variables (new/set)
	-> Single Log View
	-> Rate Setting Editor
	-> Simulation Details
	-> Debug Input

# Simulation Parameters #

```
-> Formula
	-> Active Type
	-> Open/Close Times
-> Withdraw options
-> Initial Balance
-> Symbol Rate
	-> Data Type
-> Skip Last 3
-> Skip Last X
-> Start/End Date
-> Start/End Time
	-> Activate/De-active
	-> Close All
```


# Settings #

```
-> Simulation ID
-> Symbols
	-> Pair
	-> Margin Per K
	-> Pip Value
	-> Numbers after Decimal
-> Market Open/Close Date
```